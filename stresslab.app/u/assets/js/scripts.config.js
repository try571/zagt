// captcha
function gencaptcha() {
    $("#refresh").prop("disabled", true);
    $.ajax({
        url: 'captcha/captcha.php',
        type: 'get',
        async: false,
        dataType: 'text',
        success: function(data) {
            document.getElementById('img').src = data;
            $("#refresh").prop("disabled", false);
            return false;
        }
    });
}

// register
function Register() {
    let formData = new FormData(document.getElementById("Register"));
    $("#_na").hide();
    $("#_loading").show();
    $("#_register").prop("disabled", true);
    $.ajax({
        url: "inc/register",
        type: "POST",
        contentType: false,
        cache: false,
        processData: false,
        data: formData,
        success: (r) => {
            var res = JSON.parse(r);
            // Alert
            showAlert(res[1], res[0]);
            $("#_loading").hide();
            $("#_na").show();
            $("#_register").prop("disabled", false);
            if (res[0] == "success") {
                document.getElementById("img").src =
                    `includes/extras/captcha.php?req=${Math.random() * 5000000}`;
                setTimeout(() => {
                    location.href = "login";
                }, 2000);
            } else {
                document.getElementById("img").src =
                    `includes/extras/captcha.php?req=${Math.random() * 5000000}`;
            }
            return false;
        },
        error: (err) => {
            return false;
        },
    });
}

// Login
function Login() {
    let formData = new FormData(document.getElementById("Login"));
    $("#_na").hide();
    $("#_loading").show();
    $("#_login").prop("disabled", true);
    $.ajax({
        url: "inc/login",
        type: "POST",
        contentType: false,
        cache: false,
        processData: false,
        data: formData,
        success: (r) => {
            var res = JSON.parse(r);
            // Alert
            showAlert(res[1], res[0]);
            $("#_loading").hide();
            $("#_na").show();
            $("#_login").prop("disabled", false);

            if (res[0] == "success") {
                setTimeout(() => {
                    location.href = "home";
                }, 2000);
            }
            return false;
        },
        error: (err) => {
            return false;
        },
    });
}

// Changer
function Changer() {
    let formData = new FormData(document.getElementById("Changer"));
    $("#_na").hide();
    $("#_loading").show();
    $("#_changer").prop("disabled", true);
    $.ajax({
        url: "inc/Changer",
        type: "POST",
        contentType: false,
        cache: false,
        processData: false,
        data: formData,
        success: (r) => {
            var res = JSON.parse(r);
            // Alert
            showAlert(res[1], res[0]);
            $("#_loading").hide();
            $("#_na").show();
            $("#_changer").prop("disabled", false);

            if (res[0] == "success") {
                setTimeout(() => {
                    location.href = "login";
                }, 2000);
            }
            return false;
        },
        error: (err) => {
            return false;
        },
    });
}

// Buy Plans
function BuyPlan(id) {
    let Info = {
        id: id,
        CSRF: $("#CSRF-" + id).val(),
        coupon: $("#ccode-" + id).val(),
        method: $("#method-" + id).val(),
    };
    $("#method-" + id).prop("disabled", true);
    $("#loader-" + id).show();
    checkCouponPlan($("#ccode-" + id).val());
    sleep(2000).then(() => {
        $.ajax({
            url: "inc/BuyPlan",
            type: "POST",
            contentType: false,
            cache: false,
            processData: false,
            data: JSON.stringify(Info),
            dataType: "json",
            success: (r) => {
                showAlert(r[1], r[0]);
                $("#method-" + id).prop("disabled", false);
                $("#loader-" + id).hide();
                if (r[0] == "success") {
                    setTimeout(() => {
                        if ($("#method-" + id).val() != "BAL") {
                            location.href = "transaction/" + r[2];
                        } else {
                            location.reload();
                        }
                    }, 2000);
                }
                return false;
            },
            error: (err) => {
                return false;
            },
        });
    });
}

//Buy Custom
function buyCustom() {
    let Info = {
        concu: $("#conc_").val(),
        time: $("#sec_").val(),
        lvl: $("#lvl").val(),
        api: $("#api").val(),
        months: $("#months_").val(),
        coupon: $("#ccode").val(),
        method: $("#method").val(),
        CSRF: $("#CSRF").val(),
    };
    $("#method").prop("disabled", true);
    $("#loader").show();
    $.ajax({
        url: "inc/buyCustom",
        type: "POST",
        contentType: false,
        cache: false,
        processData: false,
        data: JSON.stringify(Info),
        dataType: "json",
        success: (r) => {
            showAlert(r[1], r[0]);
            //$("#_loading").hide();
            $("#method").prop("disabled", false);
            $("#loader").hide();
            if (r[0] == "success") {
                setTimeout(() => {
                    if ($("#method").val() != "BAL") {
                        location.href = "transaction/" + r[2];
                    } else {
                        location.reload();
                    }
                }, 2000);
            }
            return false;
        },
        error: (err) => {
            return false;
        },
    });
}

//Deposit
// function Deposit() {
// let coin;
// if ($("#btc").prop("checked")) {
//   coin = "BTC";
// } else if ($("#eth").prop("checked")) {
//   coin = "ETH";
// } else if ($("#xmr").prop("checked")) {
//   coin = "XMR";
// } else if ($("#ltc").prop("checked")) {
//   coin = "LTC";
// } else if ($("#usdc").prop("checked")) {
//   coin = "USDC";
// } else if ($("#bch").prop("checked")) {
//   coin = "BCH";
// } else if ($("#usdterc20").prop("checked")) {
//   coin = "USDT.ERC20";
// } else if ($("#usdttrc20").prop("checked")) {
//   coin = "USDT.TRC20";
// } else if ($("#busd").prop("checked")) {
//   coin = "BUSD.BEP20";
// } else if ($("#doge").prop("checked")) {
//   coin = "DOGE";
// } else {
//   coin = "NULL";
// }
// let Info = {
//   Amount: $("#amount").val(),
//   Crypto: coin,
// };
// $("#_na_deposit").hide();
// $("#_loading_deposit").show();
// $("#Deposit").prop("disabled", true);

// $.ajax({
//   url: "inc/Deposit",
//   type: "POST",
//   contentType: false,
//   cache: false,
//   processData: false,
//   data: JSON.stringify(Info),
//   dataType: "json",
//   success: (r) => {
//     showAlert(r[1], r[0]);
//     $("#_na_edit").show();
//     $("#_loading_edit").hide();
//     $("#Deposit").prop("disabled", false);
//     if (r[0] == "success") {
//       setTimeout(() => {
//         location.href = "transaction/" + r[2];
//       }, 2000);
//     }
//     return false;
//   },
//   error: (err) => {
//     return false;
//   },
// });
// }

function Deposit() {
    let Info = {
        coin: $("#coin").val() || 'BTC',
        CSRF: $("#CSRF").val(),
    }
    $("#btnUpdateText").hide();
    $("#btnUpdateLoading").show();
    $("#btnUpdate").prop("disabled", true);

    $.ajax({
        url: "inc/Deposit2",
        type: "POST",
        contentType: false,
        cache: false,
        processData: false,
        data: JSON.stringify(Info),
        dataType: "json",
        success: (r) => {
            showAlert(r[1], r[0]);
            $("#btnUpdateText").show();
            $("#btnUpdateLoading").hide();
            //$("#btnUpdate").prop("disabled", false);
            if (r[0] == "success") {
                // 2 = address, 3 = src qr
                $("#address").val(r[2]);
                $("#qrCode").attr('src', r[3]);
                $("#qrCode").show();
                $("#qrLoadingState").hide();
            }
            return false;
        },
        error: (err) => {
            return false;
        },
    });
}

// Addonds
function Addons() {
    let Info = {
        type: $("#type").val(),
        host: $("#host").val(),
        period: $("#period").val(),
        secs: $("#sec_").val(),
        conc: $("#conc_").val(),
        CSRF: $("#CSRF").val(),
    };
    $("#_na").hide();
    $("#_loading").show();
    $("#addonsBtn").prop("disabled", true);
    $.ajax({
        url: "inc/Addons",
        type: "POST",
        contentType: false,
        cache: false,
        processData: false,
        data: JSON.stringify(Info),
        dataType: "json",
        success: (r) => {
            showAlert(r[1], r[0]);
            $("#_na").show();
            $("#_loading").hide();
            $("#addonsBtn").prop("disabled", false);
            if (r[0] == "success") {
                setTimeout(() => {
                    location.reload();
                }, 2000);
            }
            return false;
        },
        error: (err) => {
            return false;
        },
    });
}

// API manager
function genAPI() {
    var mnames = [];
    $('div[id="methodsdiv"] input[type="checkbox"]:checked').each((i, el) => {
        mnames.push(el.name);
    });
    var methods = mnames.join("|");
    let Info = {
        maxTime: $("#time").val(),
        maxConcu: $("#concu").val(),
        ips: $("#ips").val(),
        methods: methods,
        CSRF: $("#CSRF").val(),
    };
    $("#_na_api").hide();
    $("#_loading_api").show();
    $("#createBtn").prop("disabled", true);
    $.ajax({
        url: "inc/generateAPI",
        type: "POST",
        contentType: false,
        cache: false,
        processData: false,
        data: JSON.stringify(Info),
        dataType: "json",
        success: (r) => {
            showAlert(r[1], r[0]);
            $("#_na_api").show();
            $("#_loading_api").hide();
            $("#createBtn").prop("disabled", false);
            if (r[0] == "success") {
                setTimeout(() => {
                    location.reload();
                }, 2000);
            }
            return false;
        },
        error: (err) => {
            return false;
        },
    });
}

// Delete API
function delAPI(id) {
    let Info = {
        id: id,
        CSRF: $("#CSRF").val(),
    };
    $("#_ico-" + id).hide();
    $("#_icoLoading-" + id).show();
    $("#delBtn-" + id).prop("disabled", true);
    $.ajax({
        url: "inc/delAPI",
        type: "POST",
        contentType: false,
        cache: false,
        processData: false,
        data: JSON.stringify(Info),
        dataType: "json",
        success: (r) => {
            showAlert(r[1], r[0]);
            $("#_ico-" + id).show();
            $("#_icoLoading-" + id).hide();
            $("#delBtn-" + id).prop("disabled", false);
            if (r[0] == "success") {
                setTimeout(() => {
                    location.reload();
                }, 2000);
            }
            return false;
        },
        error: (err) => {
            return false;
        },
    });
}

//Update API
function updateAPI(id) {
    let Info = {
        id: id,
        key: $("#key-" + id).val(),
        ips: $("#ips-" + id).val(),
        status: $("#status-" + id).val(),
        CSRF: $("#CSRF").val(),
    };
    $("#_na-" + id).hide();
    $("#_loading-" + id).show();
    $("#updateBtn-" + id).prop("disabled", true);
    $.ajax({
        url: "inc/updateAPI",
        type: "POST",
        contentType: false,
        cache: false,
        processData: false,
        data: JSON.stringify(Info),
        dataType: "json",
        success: (r) => {
            showAlert(r[1], r[0]);
            $("#_na-" + id).show();
            $("#_loading-" + id).hide();
            $("#updateBtn-" + id).prop("disabled", false);
            if (r[0] == "success") {
                setTimeout(() => {
                    location.reload();
                }, 2000);
            }
            return false;
        },
        error: (err) => {
            return false;
        },
    });
}

// Change API Key
function changeKey(id) {
    let Info = {
        id: id,
        CSRF: $("#CSRF").val(),
    };
    $("#__ico-" + id).hide();
    $("#__icoLoading-" + id).show();
    $("#changelBtn-" + id).prop("disabled", true);
    $.ajax({
        url: "inc/changeKey",
        type: "POST",
        contentType: false,
        cache: false,
        processData: false,
        data: JSON.stringify(Info),
        dataType: "json",
        success: (r) => {
            showAlert(r[1], r[0]);
            $("#__ico-" + id).show();
            $("#__icoLoading-" + id).hide();
            $("#changelBtn-" + id).prop("disabled", false);
            if (r[0] == "success") {
                setTimeout(() => {
                    location.reload();
                }, 2000);
            }
            return false;
        },
        error: (err) => {
            return false;
        },
    });
}

// Start l4
function startL4() {
    var schedule = false;
    if ($("#schedule").prop("checked")) {
        schedule = true;
    }
    let host;
    let port;
    if ($("#target_subnet").val()) {
        host = $("#target_subnet").val();
        port = $("#port_subnet").val();
    } else {
        host = $("#target").val();
        port = $("#port").val();
    }
    let Info = {
        host: host,
        port: port,
        method: $("#methods").val(),
        pps: $("#pps_").val(),
        time: $("#time").val(),
        power: $("#powerslider").val(),
        subnet: $("#mask").val(),
        flags: $("#flags").val() || 'MIX',
        schedule: schedule,
        date: $("#date").val(),
        datetime: $("#d_time").val(),
        CSRF: $("#CSRF").val(),
    };
    $("#_na_l4").hide();
    $("#_loading_l4").show();
    $("#btn_l4").prop("disabled", true);
    $.ajax({
        url: "inc/startL4",
        type: "POST",
        contentType: false,
        cache: false,
        processData: false,
        data: JSON.stringify(Info),
        dataType: "json",
        success: (r) => {
            showAlert(r[1], r[0]);
            $("#_na_l4").show();
            $("#_loading_l4").hide();
            $("#btn_l4").prop("disabled", false);
            if (r[0] == "success") {
                if (!r[2]) {
                    setTimeout(() => {
                        location.reload();
                    }, 2000);
                } else {
                    Attacks();
                    $("#CSRF").val(r[2]);
                }
            }
            return false;
        },
        error: (err) => {
            return false;
        },
    });
}

// Start l4 Advance
function startL4ADV() {
    var schedule = false;
    if ($("#schedule").prop("checked")) {
        schedule = true;
    }
    let Info = {
        host: $("#target").val(),
        port: $("#port").val(),
        sport: $("#sport").val(),
        sip: $("#sip").val(),
        method: $("#methods").val(),
        protocol: $("#protocol").val(),
        pps: $("#pps_").val(),
        time: $("#time").val(),
        power: $("#powerslider").val(),
        checksum: $("#checksum").val(),
        payload: $("#payload").val(),
        handshake: $("#handshake").val(),
        syn: $("#syn").val(),
        ack: $("#ack").val(),
        psh: $("#psh").val(),
        rst: $("#rst").val(),
        fin: $("#fin").val(),
        urg: $("#urg").val(),
        sequence: $("#sequence").val(),
        acksequence: $("#acksequence").val(),
        tcpoptions: $("#tcpoptions").val(),
        CSRF: $("#CSRF").val(),
    };
    $("#_na_l4ADV").hide();
    $("#_loading_l4ADV").show();
    $("#btn_l4ADV").prop("disabled", true);
    $.ajax({
        url: "inc/startL4ADV",
        type: "POST",
        contentType: false,
        cache: false,
        processData: false,
        data: JSON.stringify(Info),
        dataType: "json",
        success: (r) => {
            showAlert(r[1], r[0]);
            $("#_na_l4ADV").show();
            $("#_loading_l4ADV").hide();
            $("#btn_l4ADV").prop("disabled", false);
            if (r[0] == "success") {
                if (!r[2]) {
                    setTimeout(() => {
                        location.reload();
                    }, 2000);
                } else {
                    Attacks();
                    $("#CSRF").val(r[2]);
                }
            }
            return false;
        },
        error: (err) => {
            return false;
        },
    });
}

// Start l7
function startL7() {
    var schedule = false;
    if ($("#schedule").prop("checked")) {
        schedule = true;
    }
    let Info = {
        host: $("#target_").val().replace(/#/g, "|").replace(/&/g, "~"),
        method: $("#methods").val(),
        rmethod: $("#reqmet").val(),
        floodType: $("#floodType").val(),
        postdata: $("#pdata").val().replace(/&/g, "~") !== "" ?
            $("#pdata").val().replace(/&/g, "~") :
            false,
        header: $("#hdata").val(),
        rps: $("#rps").val(),
        status: $("#status").val(),
        captcha: $("#captcha").val(),
        geo: $("#geo").val(),
        emulation: $("#emulation").val(),
        emulation_: $("#emulation_").val(),
        emulationTime: $("#emulationTime").val(),
        time: $("#time").val(),
        concu: $("#concu").val(),
        schedule: schedule,
        date: $("#date").val(),
        datetime: $("#d_time").val(),
        CSRF: $("#CSRF").val(),
    };
    $("#_na_l7").hide();
    $("#_loading_l7").show();
    $("#btn_l7").prop("disabled", true);
    $.ajax({
        url: "inc/startL7",
        type: "POST",
        contentType: false,
        cache: false,
        processData: false,
        data: JSON.stringify(Info),
        dataType: "json",
        success: (r) => {
            showAlert(r[1], r[0]);
            if (r[0] == "success") {
                if (r[0] == "success") {
                    Attacks();
                }
                if (schedule == false) {
                    for (var i = 1; i <= ($("#concu").val() - 1); i++) {
                        $.ajax({
                            url: "inc/startL7",
                            type: "POST",
                            contentType: false,
                            cache: false,
                            processData: false,
                            data: JSON.stringify(Info),
                            dataType: "json",
                            success: (r) => {
                                showAlert(r[1], r[0]);
                                $("#_na_l7").show();
                                $("#_loading_l7").hide();
                                $("#btn_l7").prop("disabled", false);
                                if (r[0] == "success") {
                                    Attacks();
                                }
                                return false;
                            },
                            error: (err) => {
                                return false;
                            },
                        });
                    }
                }
            }

            $("#_na_l7").show();
            $("#_loading_l7").hide();
            $("#btn_l7").prop("disabled", false);
            return false;
        },
        error: (err) => {
            return false;
        },
    });
}

// Delete API
function Stop(id) {
    let Info = {
        id: id,
    };
    $("#icoStop-" + id).hide();
    $("#icoLoading-" + id).show();
    $("#sbtn-" + id).prop("disabled", true);
    $.ajax({
        url: "inc/Stop",
        type: "POST",
        contentType: false,
        cache: false,
        processData: false,
        data: JSON.stringify(Info),
        dataType: "json",
        success: (r) => {
            showAlert(r[1], r[0]);
            $("#icoStop-" + id).show();
            $("#icoLoading-" + id).hide();
            $("#sbtn-" + id).prop("disabled", false);
            if (r[0] == "success") {
                // setTimeout(() => {
                //   location.reload();
                // }, 2000);
                Attacks();
            }
            return false;
        },
        error: (err) => {
            return false;
        },
    });
}

function StopAll(id) {
    let Info = {
        id: id,
    };
    $("#icoStop").hide();
    $("#icoLoading").show();
    $("#sbtn").prop("disabled", true);
    $.ajax({
        url: "inc/StopAll",
        type: "POST",
        contentType: false,
        cache: false,
        processData: false,
        data: JSON.stringify(Info),
        dataType: "json",
        success: (r) => {
            showAlert(r[1], r[0]);
            $("#icoStop").show();
            $("#icoLoading").hide();
            $("#sbtn").prop("disabled", false);
            if (r[0] == "success") {
                Attacks();
            }
            return false;
        },
        error: (err) => {
            return false;
        },
    });
}

function updateProfile() {
    let formData = new FormData(document.getElementById("pUpdate"));
    $("#_na_edit").hide();
    $("#_loading_edit").show();
    $("#updateBtn").prop("disabled", true);
    $.ajax({
        url: "inc/updateProfile",
        type: "POST",
        contentType: false,
        cache: false,
        processData: false,
        data: formData,
        success: function(r) {
            var res = JSON.parse(r);
            // Alert
            showAlert(res[1], res[0]);
            $("#_na_edit").show();
            $("#_loading_edit").hide();
            $("#updateBtn").prop("disabled", false);
            if (res[0] == "success") {
                setTimeout(function() {
                    location.reload();
                }, 3000);
            }
            return true;
        },
        error: function(err) {
            return false;
        },
    });
}

//Update Host(Blacklist)
function updateHost(id) {
    let Info = {
        id: id,
        host: $("#host-" + id).val(),
        CSRF: $("#CSRF").val(),
    };
    $("#btn_update-" + id).prop("disabled", true);
    $.ajax({
        url: "inc/updateHost",
        type: "POST",
        contentType: false,
        cache: false,
        processData: false,
        data: JSON.stringify(Info),
        dataType: "json",
        success: (r) => {
            showAlert(r[1], r[0]);
            $("#btn_update-" + id).prop("disabled", false);
            if (r[0] == "success") {
                setTimeout(() => {
                    location.reload();
                }, 2000);
            }
            return false;
        },
        error: (err) => {
            return false;
        },
    });
}

// Delete Blacklist
function delHost(id) {
    let Info = {
        id: id,
        CSRF: $("#CSRF").val(),
    };
    $("#_ico-" + id).hide();
    $("#_icoLoading-" + id).show();
    $("#delBtn-" + id).prop("disabled", true);
    $.ajax({
        url: "inc/delHost",
        type: "POST",
        contentType: false,
        cache: false,
        processData: false,
        data: JSON.stringify(Info),
        dataType: "json",
        success: (r) => {
            showAlert(r[1], r[0]);
            $("#_ico-" + id).show();
            $("#_icoLoading-" + id).hide();
            $("#delBtn-" + id).prop("disabled", false);
            if (r[0] == "success") {
                setTimeout(() => {
                    location.reload();
                }, 2000);
            }
            return false;
        },
        error: (err) => {
            return false;
        },
    });
}
// Renew Blacklist
function renewHost(id) {
    let Info = {
        id: id,
        CSRF: $("#CSRF").val(),
    };
    $("#__ico-" + id).hide();
    $("#__icoLoading-" + id).show();
    $("#renewBtn-" + id).prop("disabled", true);
    $.ajax({
        url: "inc/renewHost",
        type: "POST",
        contentType: false,
        cache: false,
        processData: false,
        data: JSON.stringify(Info),
        dataType: "json",
        success: (r) => {
            showAlert(r[1], r[0]);
            $("#__ico-" + id).show();
            $("#__icoLoading-" + id).hide();
            $("#renewBtn-" + id).prop("disabled", false);
            if (r[0] == "success") {
                setTimeout(() => {
                    location.reload();
                }, 2000);
            }
            return false;
        },
        error: (err) => {
            return false;
        },
    });
}
// Delete Schedule
function delSchedule(id) {
    let Info = {
        id: id,
        CSRF: $("#CSRF").val(),
    };
    $("#_ico-" + id).hide();
    $("#_icoLoading-" + id).show();
    $("#delBtn-" + id).prop("disabled", true);
    $.ajax({
        url: "inc/deleteSchedule",
        type: "POST",
        contentType: false,
        cache: false,
        processData: false,
        data: JSON.stringify(Info),
        dataType: "json",
        success: (r) => {
            showAlert(r[1], r[0]);
            $("#_ico-" + id).show();
            $("#_icoLoading-" + id).hide();
            $("#delBtn-" + id).prop("disabled", false);
            if (r[0] == "success") {
                setTimeout(() => {
                    location.reload();
                }, 2000);
            }
            return false;
        },
        error: (err) => {
            return false;
        },
    });
}

//Clear notify
function clearNotify() {
    let Info = {
        CSRF: $("#CSRF").val(),
    };
    $("#_na").hide();
    $("#_loading").show();
    $("#clearBtn").prop("disabled", true);
    $.ajax({
        url: "inc/clearNotify",
        type: "POST",
        contentType: false,
        cache: false,
        processData: false,
        data: JSON.stringify(Info),
        dataType: "json",
        success: (r) => {
            showAlert(r[1], r[0]);
            $("#_na").show();
            $("#_loading").hide();
            $("#clearBtn").prop("disabled", false);
            if (r[0] == "success") {
                setTimeout(() => {
                    location.reload();
                }, 2000);
            }
            return false;
        },
        error: (err) => {
            return false;
        },
    });
}

function rAttacks() {
    $("#rAttacks").load("includes/extras/attacks").fadeIn("slow");
}

function Attacks() {
    $("#rAttacks").load("includes/extras/attacks");
}

function getVerified() {
    $.ajax({
        url: "inc/getStats",
        type: "POST",
        contentType: false,
        cache: false,
        processData: false,
        dataType: "json",
        success: (r) => {
            if (r[0] == "Not_verified") {
                showAlert("User token expired, redirecting in 3 seconds.", "warning");
                setTimeout(() => {
                    location.href = "login";
                }, 3000);
            } else if (r[0] == "gotMessage") {
                $("#messageModal").modal("show");
                $("#getPrivateMessage").html(r[1]);
            } else if (r[0] == "gotFunds") {
                $("#fundsModal").modal("show");
                $("#fundsbox").html(r[2]);
                $("#messagebox").html(r[1]);
            } else if (r[0] == "gotKicked") {
                showAlert(
                    "You were kicked from the site, redirecting in 3 seconds.",
                    "warning"
                );
                setTimeout(() => {
                    location.href = "login";
                }, 60000);
            } else if (r[0] == "Verified") {
                $("#onlineUsers").html(r[1]);
            }
        },
        error: (err) => {
            return false;
        },
    });
}

function getVerifiedInvoice() {
    $.ajax({
        url: "../inc/getStats",
        type: "POST",
        contentType: false,
        cache: false,
        processData: false,
        dataType: "json",
        success: (r) => {
            if (r[0] == "Not_verified") {
                showAlert("User token expired, redirecting in 3 seconds.", "warning");
                setTimeout(() => {
                    location.href = "login";
                }, 3000);
            } else if (r[0] == "gotMessage") {
                $("#messageModal").modal("show");
                $("#getPrivateMessage").html(r[1]);
            } else if (r[0] == "gotFunds") {
                $("#fundsModal").modal("show");
                $("#fundsbox").html(r[2]);
                $("#messagebox").html(r[1]);
            } else if (r[0] == "gotKicked") {
                showAlert(
                    "You were kicked from the site, redirecting in 3 seconds.",
                    "warning"
                );
                setTimeout(() => {
                    location.href = "login";
                }, 3000);
            } else if (r[0] == "Verified") {
                $("#onlineUsers").html(r[1]);
            }
        },
        error: (err) => {
            return false;
        },
    });
}

//get price
function getTotal(price, discount) {
    var value = 0;
    if (discount) {
        value = price * discount;
        value = discount >= 1 ? 0 : price - value;
        return value;
    }
    return price;
}

//verify coupons set
function checkCoupon() {
    let ccode = $("#ccode").val();
    var price = 0;
    var price_discount = 0;
    price =
        ($("#sec_").val() * 12 -
            12 +
            ($("#conc_").val() * 50 - 50) +
            $("#lvl").val() * 70 +
            $("#api").val() * 50 +
            30) *
        $("#months_").val();
    if ($("#months_").val() > 1) {
        price_discount = getTotal(price, 0.05);
    } else {
        price_discount = price;
    }

    if (!ccode) {
        $("#builder_modal").modal("hide");
        $("#display_total").html("$" + price_discount);
        $("#confirmation_modal").modal("show");
    } else {
        showAlert("Checking Coupon Code", "info");
        sleep(3000).then(() => {
            let Info = {
                code: ccode,
                CSRF: $("#CSRF").val(),
            };
            $.ajax({
                url: "inc/checkCoupon",
                type: "POST",
                contentType: false,
                cache: false,
                processData: false,
                data: JSON.stringify(Info),
                dataType: "json",
                success: (r) => {
                    showAlert(r[1], r[0]);
                    $("#_na").show();
                    $("#_loading").hide();
                    $("#Deposit").prop("disabled", false);
                    if (r[0] == "success") {
                        setTimeout(() => {
                            $("#builder_modal").modal("hide");
                            price_discount =
                                $("#months_").val() > 1 ?
                                getTotal(price, 0.05 + r[2]) :
                                getTotal(price, r[2]);
                            $("#display_total").html("$" + price_discount);
                            $("#discoun2").show();
                            $("#display_dis1").html(
                                '<span class="badge badge-light-success">-' + r[3] + "%</span>"
                            );
                            $("#confirmation_modal").modal("show");
                            exist = true;
                        }, 2000);
                    } else {
                        $("#builder_modal").modal("hide");
                        $("#display_total").html("$" + price_discount);
                        $("#confirmation_modal").modal("show");
                    }
                    return false;
                },
                error: (err) => {
                    return false;
                },
            });
        });
    }
}

function checkCouponPlan(ccode) {
    var price_discount = 0;
    if (!ccode) {
        //empty response
    } else {
        showAlert("Checking Coupon Code", "info");
        sleep(2000).then(() => {
            let Info = {
                code: ccode,
                CSRF: $("#CSRF").val(),
            };
            $.ajax({
                url: "inc/checkCoupon",
                type: "POST",
                contentType: false,
                cache: false,
                processData: false,
                data: JSON.stringify(Info),
                dataType: "json",
                success: (r) => {
                    showAlert(r[1], r[0]);
                    if (r[0] == "success") {
                        //me
                    }
                    return false;
                },
                error: (err) => {
                    return false;
                },
            });
        });
    }
}

// alerts
function showAlert(txt, type) {
    if (type == "error") {
        toastr.options = {
            closeButton: true,
            debug: false,
            newestOnTop: true,
            positionClass: "toast-top-right",
            preventDuplicates: true,
            onclick: null,
            showDuration: "1000",
            hideDuration: "1000",
            timeOut: "5000",
            extendedTimeOut: "1000",
            showEasing: "swing",
            hideEasing: "linear",
            showMethod: "fadeIn",
            hideMethod: "fadeOut",
        };
        toastr.error(txt, type.toUpperCase());
    } else if (type == "info") {
        toastr.options = {
            closeButton: true,
            debug: false,
            newestOnTop: true,
            positionClass: "toast-top-right",
            preventDuplicates: true,
            onclick: null,
            showDuration: "1000",
            hideDuration: "1000",
            timeOut: "5000",
            extendedTimeOut: "1000",
            showEasing: "swing",
            hideEasing: "linear",
            showMethod: "fadeIn",
            hideMethod: "fadeOut",
        };
        toastr.info(txt, type.toUpperCase());
    } else if (type == "warning") {
        toastr.options = {
            closeButton: true,
            debug: false,
            newestOnTop: true,
            positionClass: "toast-top-right",
            preventDuplicates: true,
            onclick: null,
            showDuration: "1000",
            hideDuration: "1000",
            timeOut: "5000",
            extendedTimeOut: "1000",
            showEasing: "swing",
            hideEasing: "linear",
            showMethod: "fadeIn",
            hideMethod: "fadeOut",
        };
        toastr.warning(txt, type.toUpperCase());
    } else if (type == "success") {
        toastr.options = {
            closeButton: true,
            debug: false,
            newestOnTop: true,
            positionClass: "toast-top-right",
            preventDuplicates: true,
            onclick: null,
            showDuration: "1000",
            hideDuration: "1000",
            timeOut: "5000",
            extendedTimeOut: "1000",
            showEasing: "swing",
            hideEasing: "linear",
            showMethod: "fadeIn",
            hideMethod: "fadeOut",
        };
        toastr.success(txt, type.toUpperCase());
    } else {
        console.log("wrong alert type");
    }
}

function sleep(time) {
    return new Promise((resolve) => setTimeout(resolve, time));
}